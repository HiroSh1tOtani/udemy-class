import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Heroe } from '../../interfaces/heroe';

import { HeroesService } from '../../services/heroes.service';


@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
})
export class HeroeComponent {

  heroe:Heroe;

  constructor( private _activatedRoute: ActivatedRoute,
               private _heroesService: HeroesService ) {

    _activatedRoute.params.subscribe( params=>{
      this.heroe = _heroesService.getHeroe( params.id );
    })

  }

}
