// concatenacion de cadenas
function getNombre(): number{
  return 2+1;
}

let nombre:String = "Juan";
let apellido:string = "Perez";
let edad:number = 32;

// let texto = "Hola, " + nombre + " " + apellido + "("+ edad + ")";
let texto = `Hola,
${ nombre } ${ apellido }
(${ edad })`;

console.log(texto);

let texto2:string = ` ${ getNombre() } `;

console.log("Concatenacion de Cadenas: "+texto2);

//Parametros opcionales, obligatorios y por defecto
function activar( quien:string,
                  momento?:string,
                  objeto:string = "batiseñal"
                  ){

  let mensaje:string;

  if( momento ){
    mensaje = `${ quien } activó la ${ objeto } en la ${ momento }`;
  }else{
    mensaje = `${ quien } activó la ${ objeto }`;
  }

  console.log("Parametros opcionales, obligatorios y por defecto: "+mensaje);

}

activar("Gordon");

//Funciones de Flecha
let miFuncion = function( a:any ){
  return a;
}

let mifuncionF = ( a:any ) => a;


let miFuncion2 = function( a:number, b:number ){
  return a + b;
}

let miFuncion2F = ( a:number, b:number ) => a + b;


let miFuncion3 = function( nombre:string ){
  nombre = nombre.toUpperCase();
  return nombre;
}

let miFuncion3F = ( nombre:string )=>{
  nombre = nombre.toUpperCase();
  return nombre;
}


let hulk = {
  nombre: "Hulk",
  smash(){

    setTimeout( ()=> console.log(this.nombre + " smash!!") , 1500 );

  }
}

hulk.smash();

//Destructuracion de Objetos y Arreglos
//Objetos
let avenger = {
  heroe: "steve",
  clave: "Capitan America",
  poder: "Droga"
}

let { heroe, clave, poder } = avenger;

console.log( heroe, clave, poder );

//Arreglos
let avengers:string[] = [ "Thor","Steve","Tony" ];

let [ , , ironman ] = avengers;

console.log( ironman );

//Promesas en ES6
let prom1 = new Promise( function( resolve: any, reject: any ){

  setTimeout( ()=>{
    console.log("Promesa terminada");

    // Termina bien
    //resolve();

    // Termina mal
     reject()

  }, 1500 )

})

console.log("Paso 1");

prom1.then( function(){
  console.log("Ejecutarme cuando se termine bien!");
},
  function(){
    console.error("Ejecutar si todo sale mal");
  }
)

console.log("Paso 2");

//Interfaces de TypeScript

interface Xmen {
  nombre:string,
  poder:string
}

function enviarMision(xmen: Xmen){

  console.log("Enviando a: " + xmen.nombre);

};

function enviarCuartel(xmen: Xmen){

  console.log("Enviando al cuartel: " + xmen.nombre);

};

let wolverine:Xmen = {
  nombre: "wolverine",
  poder: "Regeneración"
};

enviarMision(wolverine);
enviarCuartel(wolverine);