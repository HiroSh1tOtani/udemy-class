"use strict";
// concatenacion de cadenas
function getNombre() {
    return 2 + 1;
}
var nombre = "Juan";
var apellido = "Perez";
var edad = 32;
// let texto = "Hola, " + nombre + " " + apellido + "("+ edad + ")";
var texto = "Hola,\n" + nombre + " " + apellido + "\n(" + edad + ")";
console.log(texto);
var texto2 = " " + getNombre() + " ";
console.log("Concatenacion de Cadenas: " + texto2);
//Parametros opcionales, obligatorios y por defecto
function activar(quien, momento, objeto) {
    if (objeto === void 0) { objeto = "batiseñal"; }
    var mensaje;
    if (momento) {
        mensaje = quien + " activ\u00F3 la " + objeto + " en la " + momento;
    }
    else {
        mensaje = quien + " activ\u00F3 la " + objeto;
    }
    console.log("Parametros opcionales, obligatorios y por defecto: " + mensaje);
}
activar("Gordon");
//Funciones de Flecha
var miFuncion = function (a) {
    return a;
};
var mifuncionF = function (a) { return a; };
var miFuncion2 = function (a, b) {
    return a + b;
};
var miFuncion2F = function (a, b) { return a + b; };
var miFuncion3 = function (nombre) {
    nombre = nombre.toUpperCase();
    return nombre;
};
var miFuncion3F = function (nombre) {
    nombre = nombre.toUpperCase();
    return nombre;
};
var hulk = {
    nombre: "Hulk",
    smash: function () {
        var _this = this;
        setTimeout(function () { return console.log(_this.nombre + " smash!!"); }, 1500);
    }
};
hulk.smash();
//Destructuracion de Objetos y Arreglos
//Objetos
var avenger = {
    heroe: "steve",
    clave: "Capitan America",
    poder: "Droga"
};
var heroe = avenger.heroe, clave = avenger.clave, poder = avenger.poder;
console.log(heroe, clave, poder);
//Arreglos
var avengers = ["Thor", "Steve", "Tony"];
var ironman = avengers[2];
console.log(ironman);
//Promesas en ES6
var prom1 = new Promise(function (resolve, reject) {
    setTimeout(function () {
        console.log("Promesa terminada");
        // Termina bien
        //resolve();
        // Termina mal
        reject();
    }, 1500);
});
console.log("Paso 1");
prom1.then(function () {
    console.log("Ejecutarme cuando se termine bien!");
}, function () {
    console.error("Ejecutar si todo sale mal");
});
console.log("Paso 2");
function enviarMision(xmen) {
    console.log("Enviando a: " + xmen.nombre);
}
;
function enviarCuartel(xmen) {
    console.log("Enviando al cuartel: " + xmen.nombre);
}
;
var wolverine = {
    nombre: "wolverine",
    poder: "Regeneración"
};
enviarMision(wolverine);
enviarCuartel(wolverine);
