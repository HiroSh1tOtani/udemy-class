import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clases',
  templateUrl: './clases.component.html'
})
export class ClasesComponent {

  alerta: string;
  loading: boolean;

  propiedades: Object;

  constructor() {
    this.alerta = 'alert-danger';
    this.loading = false;
    this.propiedades = {
      danger: false
    };
  }

  ejecutar() {

    this.loading = true;

    setTimeout(() => this.loading = false, 3000);

  }

}
