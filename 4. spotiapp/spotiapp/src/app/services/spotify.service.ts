import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { }

  getQuery( query: string ) {

    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQDG9gIpeOrjb3wrZLj30eMfgZtZUlVv7jSfgQv0gRKtpUCziyc-UyO-9_Q2gz-qt2m3DATgM8_OWGaFVA8'
    });

    return this.http.get(url, { headers });

  }

  getNewReleases() {

    return this.getQuery(`browse/new-releases?limit=20`)
              .pipe( map( (data: any) => data.albums.items ));

  }

  getArtistas( termino: string ) {

    return this.getQuery(`search?query=${ termino }&type=artist&limit=15`)
              .pipe( map( (data: any) => data.artists.items ));

  }

  getArtista( id: string ) {

    return this.getQuery(`artists/${ id }`);

  }

  getTopTracks( id: string ) {

    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
              .pipe( map( (data: any) => data.tracks ));

  }

}
